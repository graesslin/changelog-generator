/********************************************************************
Copyright (C) 2012 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#ifndef ChangelogGenerator_H
#define ChangelogGenerator_H

#include <QtCore/QObject>
#include <QtCore/QUrl>

class Bug;
class QNetworkReply;
class QNetworkAccessManager;

class ChangelogGenerator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString bugtracker READ bugtracker WRITE setBugtracker)
    Q_PROPERTY(QString product READ product WRITE setProduct)
    Q_PROPERTY(QString version READ version WRITE setVersion)
    Q_PROPERTY(QString targetMilestone READ targetMilestone WRITE setTargetMilestone)
    Q_PROPERTY(QString module READ module WRITE setModule)
    Q_PROPERTY(QString repository READ repository WRITE setRepository)
public:
    enum class Mode {
        Changelog,
        FeaturePlan
    };
    ChangelogGenerator(Mode mode, QObject *parent = nullptr);
    virtual ~ChangelogGenerator();

    const QString &bugtracker() {
        return m_bugtracker;
    }
    const QString &product() const {
        return m_product;
    }
    const QString &version() const {
        return m_version;
    }
    const QString &targetMilestone() const {
        return m_targetMilestone;
    }
    const QString &module() const {
        return m_module;
    }
    const QString &repository() const {
        return m_repository;
    }

    void setBugtracker(const QString &bugtracker) {
        m_bugtracker = bugtracker;
    }
    void setProduct(const QString &product) {
        m_product = product;
    }
    void setVersion(const QString &version) {
        m_version = version;
    }
    void setTargetMilestone(const QString &targetMilestone) {
        m_targetMilestone = targetMilestone;
    }
    void setModule(const QString &module) {
        m_module = module;
    }
    void setRepository(const QString &repository) {
        m_repository = repository;
    }

    void start();
private slots:
    void loadFinished(QNetworkReply *reply);

private:
    void bugListLoaded(QNetworkReply *reply);
    void bugsLoaded(QNetworkReply *reply);
    void commentsLoaded(QNetworkReply *reply);
    QUrl bugListQuery();
    void loadBugs(const QStringList &bugIds);
    void loadCommentsForBugs();
    void generateChangelog();
    void generateFeaturePlan();
    void finalize();
    QString m_bugtracker;
    QString m_product;
    QString m_version;
    QString m_targetMilestone;
    QString m_module;
    QString m_repository;
    Mode m_mode;
    QList<Bug*> m_bugs;
    QNetworkAccessManager *m_manager;
};

#endif // ChangelogGenerator_H
