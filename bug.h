/********************************************************************
Copyright (C) 2012 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#ifndef BUG_H
#define BUG_H

#include <QtCore/QObject>
#include <QtCore/QVariantList>

class Bug : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString alias READ alias WRITE setAlias)
    Q_PROPERTY(QString assigned_to READ assignedTo WRITE setAssignedTo)
    Q_PROPERTY(QVariantList blocks READ blocks WRITE setBlocks)
    Q_PROPERTY(QVariantList cc READ cc WRITE setCc)
    Q_PROPERTY(QString cf_versionfixedin READ versionFixedIn WRITE setVersionFixedIn)
    Q_PROPERTY(QString cf_commitlink READ commitLink WRITE setCommitLink)
    Q_PROPERTY(QString classification READ classification WRITE setClassification)
    Q_PROPERTY(QString component READ component WRITE setComponent)
    Q_PROPERTY(QString creation_time READ creationTime WRITE setCreationTime)
    Q_PROPERTY(QString creator READ creator WRITE setCreator)
    Q_PROPERTY(QVariantList depends_on READ dependsOn WRITE setDependsOn)
    Q_PROPERTY(QString dupe_of READ dupeOf WRITE setDupeOf)
    Q_PROPERTY(QVariantList groups READ groups WRITE setGroups)
    Q_PROPERTY(qulonglong id READ id WRITE setId)
    Q_PROPERTY(bool is_cc_accessible READ isCcAccessible WRITE setCcAccessible)
    Q_PROPERTY(bool is_confirmed READ isConfirmed WRITE setConfirmed)
    Q_PROPERTY(bool is_creator_accessible READ isCreatorAccessible WRITE setCreatorAccessible)
    Q_PROPERTY(bool is_open READ isOpen WRITE setOpen)
    Q_PROPERTY(QVariantList keywords READ keywords WRITE setKeywords)
    Q_PROPERTY(QString last_change_time READ lastChangeTime WRITE setLastChangeTime)
    Q_PROPERTY(QString op_sys READ operatingSystem WRITE setOperatingSystem)
    Q_PROPERTY(QString platform READ platform WRITE setPlatform)
    Q_PROPERTY(QString priority READ priority WRITE setPriority)
    Q_PROPERTY(QString product READ product WRITE setProduct)
    Q_PROPERTY(QString qa_contact READ qaContact WRITE setQaContact)
    Q_PROPERTY(QString resolution READ resolution WRITE setResolution)
    Q_PROPERTY(QVariantList see_also READ seeAlso WRITE setSeeAlso)
    Q_PROPERTY(QString severity READ severity WRITE setSeverity)
    Q_PROPERTY(QString status READ status WRITE setStatus)
    Q_PROPERTY(QString summary READ summary WRITE setSummary)
    Q_PROPERTY(QString target_milestone READ targetMilestone WRITE setTargetMilestone)
    Q_PROPERTY(QString url READ url WRITE setUrl)
    Q_PROPERTY(QString version READ version WRITE setVersion)
    Q_PROPERTY(QString whiteboard READ whiteboard WRITE setWhiteboard)

public:
    Bug(QObject *parent = nullptr);
    virtual ~Bug();
    const QString &alias() const {
        return m_alias;
    }
    const QString &assignedTo() const {
        return m_assignedTo;
    }
    const QVariantList &blocks() const {
        return m_blocks;
    }
    const QVariantList &cc() const {
        return m_cc;
    }
    const QString &versionFixedIn() const {
        return m_versionFixedIn;
    }
    const QString &commitLink() const {
        return m_commitLink;
    }
    const QString &classification() const {
        return m_classification;
    }
    const QString &component() const {
        return m_component;
    }
    const QString &creationTime() const {
        return m_creationTime;
    }
    const QString &creator() const {
        return m_creator;
    }
    const QVariantList &dependsOn() const {
        return m_dependsOn;
    }
    const QString &dupeOf() const {
        return m_dupeOf;
    }
    const QVariantList &groups() const {
        return m_groups;
    }
    qulonglong id() const {
        return m_id;
    }
    bool isCcAccessible() const {
        return m_ccAccessible;
    }
    bool isConfirmed() const {
        return m_confirmed;
    }
    bool isCreatorAccessible() const {
        return m_creatorAccessible;
    }
    bool isOpen() const {
        return m_open;
    }
    const QVariantList &keywords() const {
        return m_keywords;
    }
    const QString &lastChangeTime() const {
        return m_lastChangeTime;
    }
    const QString &operatingSystem() const {
        return m_operatingSystem;
    }
    const QString &platform() const {
        return m_platform;
    }
    const QString &priority() const {
        return m_priority;
    }
    const QString &product() const {
        return m_product;
    }
    const QString &qaContact() const {
        return m_qaContact;
    }
    const QString &resolution() const {
        return m_resolution;
    }
    const QVariantList &seeAlso() const {
        return m_seeAlso;
    }
    const QString &severity() const {
        return m_severity;
    }
    const QString &status() const {
        return m_status;
    }
    const QString &summary() const {
        return m_summary;
    }
    const QString &targetMilestone() const {
        return m_targetMilestone;
    }
    const QString &url() const {
        return m_url;
    }
    const QString &version() const {
        return m_version;
    }
    const QString &whiteboard() const {
        return m_whiteboard;
    }

    void setAlias(const QString &alias);
    void setAssignedTo(const QString &assignedTo);
    void setBlocks(const QVariantList &blocks);
    void setCc(const QVariantList &cc);
    void setVersionFixedIn(const QString &versionFixedIn);
    void setCommitLink(const QString &commitLink);
    void setClassification(const QString &classification);
    void setComponent(const QString &component);
    void setCreationTime(const QString &creationTime);
    void setCreator(const QString &creator);
    void setDependsOn(const QVariantList &dependsOn);
    void setDupeOf(const QString &dupeOf);
    void setGroups(const QVariantList &groups);
    void setId(qulonglong id);
    void setCcAccessible(bool set);
    void setConfirmed(bool set);
    void setCreatorAccessible(bool set);
    void setOpen(bool set);
    void setKeywords(const QVariantList &keywords);
    void setLastChangeTime(const QString &lastChangeTime);
    void setOperatingSystem(const QString &operatingSystem);
    void setPlatform(const QString &platform);
    void setPriority(const QString &priority);
    void setProduct(const QString &product);
    void setQaContact(const QString &qaContact);
    void setResolution(const QString &resolution);
    void setSeeAlso(const QVariantList &seeAlso);
    void setSeverity(const QString &severity);
    void setStatus(const QString &status);
    void setSummary(const QString &summary);
    void setTargetMilestone(const QString &targetMilestone);
    void setUrl(const QString &url);
    void setVersion(const QString &version);
    void setWhiteboard(const QString &whiteboard);

    QString commitHash() const;
    QString reviewId() const;

private:
    QString m_alias;
    QString m_assignedTo;
    QVariantList m_blocks;
    QVariantList m_cc;
    QString m_versionFixedIn;
    QString m_commitLink;
    QString m_classification;
    QString m_component;
    // TODO: QDate
    QString m_creationTime;
    QString m_creator;
    QVariantList m_dependsOn;
    QString m_dupeOf;
    QVariantList m_groups;
    qulonglong m_id;
    bool m_ccAccessible;
    bool m_confirmed;
    bool m_creatorAccessible;
    bool m_open;
    QVariantList m_keywords;
    QString m_lastChangeTime;
    QString m_operatingSystem;
    QString m_platform;
    QString m_priority;
    QString m_product;
    QString m_qaContact;
    QString m_resolution;
    QVariantList m_seeAlso;
    QString m_severity;
    QString m_status;
    QString m_summary;
    QString m_targetMilestone;
    QString m_url;
    QString m_version;
    QString m_whiteboard;
};

#endif // BUG_H
