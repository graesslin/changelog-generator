/********************************************************************
Copyright (C) 2012 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "ChangelogGenerator.h"
#include "comment.h"
#include "bug.h"

// QJson
#include <qjson/qobjecthelper.h>
#include <qjson/parser.h>
#include <qjson/serializer.h>

// Qt
#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QUrl>
#include <QtCore/QStringList>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtXml/QXmlStreamReader>

// STL
#include <algorithm>

ChangelogGenerator::ChangelogGenerator(Mode mode, QObject *parent)
    : QObject(parent)
    , m_bugtracker()
    , m_product()
    , m_version()
    , m_targetMilestone()
    , m_module()
    , m_repository()
    , m_mode(mode)
    , m_manager(new QNetworkAccessManager(this))
{
    connect(m_manager, SIGNAL(finished(QNetworkReply*)), SLOT(loadFinished(QNetworkReply*)));
}

ChangelogGenerator::~ChangelogGenerator()
{}

void ChangelogGenerator::start()
{
    QNetworkReply *reply = m_manager->get(QNetworkRequest(bugListQuery()));
    reply->setProperty("requestType", "bugsearch");
}

void ChangelogGenerator::loadFinished(QNetworkReply* reply)
{
    QVariant type = reply->property("requestType");
    if (!type.isValid()) {
        qDebug() << "requestType for network request not found";
        qApp->quit();
        return;
    }
    if (type.toString() == "bugsearch") {
        bugListLoaded(reply);
    } else if (type.toString() == "bugs") {
        bugsLoaded(reply);
    } else if (type.toString() == "comments") {
        commentsLoaded(reply);
    } else {
        qDebug() << "Unknown requestType for network request found";
        qApp->quit();
    }
    reply->deleteLater();
}

QUrl ChangelogGenerator::bugListQuery()
{
    QUrl bugtracker(m_bugtracker);
    bugtracker.setPath("buglist.cgi");
    QList<QPair<QString, QString>> query;
    query << QPair<QString, QString>("ctype", "rdf");
    if (m_mode == Mode::Changelog) {
        query << QPair<QString, QString>("resolution", "FIXED");
        query << QPair<QString, QString>("bug_status", "RESOLVED");
    }
    if (!m_version.isNull()) {
        query << QPair<QString, QString>("chfieldto", "Now");
        query << QPair<QString, QString>("chfield", "cf_versionfixedin");
        query << QPair<QString, QString>("chfieldvalue", m_version);
    }
    if (!m_targetMilestone.isNull()) {
        query << QPair<QString, QString>("target_milestone", m_targetMilestone);
    }
    query << QPair<QString, QString>("product", m_product);
    bugtracker.setQueryItems(query);
    return bugtracker;
}

void ChangelogGenerator::bugListLoaded(QNetworkReply* reply)
{
    QStringList bugIds;
    QXmlStreamReader xml(reply);
    while (!xml.atEnd()) {
        if (xml.readNext() == QXmlStreamReader::StartElement) {
            if (xml.namespaceUri() == "http://www.bugzilla.org/rdf#" && xml.name() == "id") {
                bugIds << xml.readElementText();
            }
        }
    }
    loadBugs(bugIds);
}

void ChangelogGenerator::loadBugs(const QStringList& bugIds)
{
    QUrl bugtracker(m_bugtracker);
    bugtracker.setPath("jsonrpc.cgi");
    QVariantMap args;
    QVariantList bugs;
    std::for_each(bugIds.constBegin(), bugIds.constEnd(), [&](const QString &id) {
        bugs << id.toInt();
    });
    args.insert("ids", bugs);
    QJson::Serializer serializer;
    QByteArray serializedData = serializer.serialize(QVariantList() << args);
    bugtracker.setQueryItems(QList<QPair<QString, QString>>() << QPair<QString, QString>("method", "Bug.get") << QPair<QString, QString>("params", serializedData));
    QNetworkReply *reply = m_manager->get(QNetworkRequest(bugtracker));
    reply->setProperty("requestType", "bugs");
}

void ChangelogGenerator::bugsLoaded(QNetworkReply *reply)
{
    bool ok = false;
    QJson::Parser parser;
    QVariantMap parsedData = parser.parse(reply, &ok).toMap();
    if (!ok) {
        qDebug() << "Parsing of JSON result failed";
        qApp->quit();
        return;
    }
    if (!parsedData["error"].isNull()) {
        QVariantMap error = parsedData["error"].toMap();
        qDebug() << "Error while loading Bug list: " << error["message"].toString() << "(" << error["code"].toString() << ")";
        qApp->quit();
        return;
    }
    QVariantList bugs = parsedData["result"].toMap().value("bugs").toList();
    for (const auto &bug : bugs) {
        Bug *bugObject = new Bug(this);
        QJson::QObjectHelper::qvariant2qobject(bug.toMap(), bugObject);
        m_bugs << bugObject;
    }
    loadCommentsForBugs();
}

void ChangelogGenerator::loadCommentsForBugs()
{
    QUrl bugtracker(m_bugtracker);
    bugtracker.setPath("jsonrpc.cgi");
    QVariantMap args;
    QVariantList bugs;
    std::for_each(m_bugs.constBegin(), m_bugs.constEnd(), [&](const Bug *bug) {
        if (bug->commitLink().isEmpty()) {
            // only load comments for bugs where we do not have the commit link set
            bugs << bug->id();
        }
    });
    if (bugs.isEmpty()) {
        finalize();
        return;
    }
    args.insert("ids", bugs);
    QJson::Serializer serializer;
    QByteArray serializedData = serializer.serialize(QVariantList() << args);
    bugtracker.setQueryItems(QList<QPair<QString, QString>>() << QPair<QString, QString>("method", "Bug.comments") << QPair<QString, QString>("params", serializedData));
    QNetworkReply *reply = m_manager->get(QNetworkRequest(bugtracker));
    reply->setProperty("requestType", "comments");
}

void ChangelogGenerator::commentsLoaded(QNetworkReply* reply)
{
    bool ok = false;
    QJson::Parser parser;
    QVariantMap parsedData = parser.parse(reply, &ok).toMap();
    if (!ok) {
        qDebug() << "Parsing of JSON result failed";
        qApp->quit();
        return;
    }
    if (!parsedData["error"].isNull()) {
        QVariantMap error = parsedData["error"].toMap();
        qDebug() << "Error while loading Bug list: " << error["message"].toString() << "(" << error["code"].toString() << ")";
        qApp->quit();
        return;
    }
    QVariantMap bugs = parsedData["result"].toMap().value("bugs").toMap();
    auto findBug = [&](qulonglong bugNo) -> Bug* {
        for (auto bug : m_bugs) {
            if (bug->id() == bugNo) {
                return bug;
            }
        }
        return nullptr;
    };
    for (auto it = bugs.constBegin(); it != bugs.constEnd(); ++it) {
        Bug *bug = findBug(it.key().toLongLong());
        if (!bug) {
            qDebug() << "Could not find bug with ID " << it.key();
            qApp->quit();
            return;
        }
        QVariantList comments = it.value().toMap().value("comments").toList();
        for (QVariant comment : comments) {
            Comment *c = new Comment(bug);
            QJson::QObjectHelper::qvariant2qobject(comment.toMap(), c);
        }
    }
    finalize();
}

void ChangelogGenerator::finalize()
{
    if (m_mode == Mode::Changelog) {
        generateChangelog();
    } else if (m_mode == Mode::FeaturePlan) {
        generateFeaturePlan();
    }

    qApp->quit();
}

void ChangelogGenerator::generateChangelog()
{
    QByteArray output;
    QXmlStreamWriter writer(&output);
    writer.setAutoFormatting(true);
    writer.writeStartDocument();

    writer.writeStartElement("changelog");

    writer.writeStartElement("release");
    writer.writeAttribute("version", m_version);

    writer.writeStartElement("module");
    if (!m_module.isNull()) {
        writer.writeAttribute("name", m_module);
    }
    if (!m_repository.isNull()) {
        writer.writeAttribute("reponame", m_repository);
    }

    writer.writeStartElement("product");
    writer.writeAttribute("name", m_product);
    std::for_each(m_bugs.constBegin(), m_bugs.constEnd(), [&](const Bug* bug) {
        if (bug->isOpen()) {
            return;
        }
        writer.writeStartElement(bug->severity() == "wishlist" ? "feature" : "bugfix");
        writer.writeAttribute("bugno", QString::number(bug->id()));
        writer.writeAttribute("class", bug->severity());
        const QString commit = bug->commitHash();
        if (!commit.isNull()) {
            writer.writeAttribute("rev", commit);
        }
        writer.writeCharacters(bug->summary());
        writer.writeEndElement();
    });
    writer.writeEndElement(); // product
    writer.writeEndElement(); // module
    writer.writeEndElement(); // release
    writer.writeEndElement(); // changelog
    writer.writeEndDocument();
    qDebug() << output;
}

void ChangelogGenerator::generateFeaturePlan()
{
    QByteArray data;
    QTextStream output(&data);
    if (!m_module.isNull()) {
        output << "= " << m_module << " =\n";
    }
    output << "{| cellspacing=\"0\" cellpadding=\"5\" border=\"1\" style=\"border: 1px solid gray; border-collapse: collapse; text-align: left; width: 100%;\" class=\"sortable\"\n";
    output << "|- style=\"background: rgb(236, 236, 236) none repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous; white-space: nowrap;\"\n";
    output << "! Status\n";
    output << "! Project\n";
    output << "! Description\n";
    output << "! Contact\n";
    output << "<!-- The following section of entries has been auto generated by ChangelogGenerator. Do not edit!\n";
    output << "BEGIN GENERATED SECTION -->\n";
    std::for_each(m_bugs.constBegin(), m_bugs.constEnd(), [&](const Bug *bug) {
        output << "{{";
        const QString review = bug->reviewId();
        if (bug->isOpen()) {
            if (!review.isNull()) {
                output << "FeatureInProgress";
            } else {
                output << "FeatureTodo";
            }
        } else {
            output << "FeatureDone";
        }
        output << "|"  << m_product;
        QString summary = bug->summary();
        summary.replace("|", "<nowiki>|</nowiki>");
        output << "|"  << summary << " ({{bug |" << bug->id() << "}}";
        if (!review.isNull()) {
            output << ", Review " << review;
        }
        output << ")";
        output << "|"  << bug->assignedTo();
        output << "}}\n";
    });
    output << "<!-- END GENERATED SECTION -->\n";
    output << "|}\n";
    output.flush();
    qDebug() << data;
}

#include "ChangelogGenerator.moc"
