/********************************************************************
Copyright (C) 2012 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "bug.h"
#include "comment.h"

#include <QtCore/QDate>
#include <QtCore/QStringList>
#include <QtCore/QRegExp>

auto stringSetter = [](QString &variable, const QString &value) {
    if (variable == value) {
        return false;
    }
    variable = value;
    return true;
};

auto variantListSetter = [](QVariantList &variable, const QVariantList &value) {
    if (variable == value) {
        return false;
    }
    variable = value;
    return true;
};

auto boolSetter = [](bool &variable, bool value) {
    if (variable == value) {
        return false;
    }
    variable = value;
    return true;
};

Bug::Bug(QObject* parent)
    : QObject(parent)
    , m_alias()
    , m_assignedTo()
    , m_blocks()
    , m_cc()
    , m_versionFixedIn()
    , m_commitLink()
    , m_classification()
    , m_component()
    , m_creationTime()
    , m_creator()
    , m_dependsOn()
    , m_dupeOf()
    , m_groups()
    , m_id(0)
    , m_ccAccessible(false)
    , m_confirmed(false)
    , m_creatorAccessible(false)
    , m_open(false)
    , m_keywords()
    , m_lastChangeTime()
    , m_operatingSystem()
    , m_platform()
    , m_priority()
    , m_product()
    , m_qaContact()
    , m_resolution()
    , m_seeAlso()
    , m_severity()
    , m_status()
    , m_summary()
    , m_targetMilestone()
    , m_url()
    , m_version()
    , m_whiteboard()
{
}

Bug::~Bug()
{
}

void Bug::setAlias(const QString& alias)
{
    stringSetter(m_alias, alias);
}

void Bug::setAssignedTo(const QString& assignedTo)
{
    stringSetter(m_assignedTo, assignedTo);
}

void Bug::setBlocks(const QVariantList& blocks)
{
    variantListSetter(m_blocks, blocks);
}

void Bug::setCc(const QVariantList& cc)
{
    variantListSetter(m_cc, cc);
}

void Bug::setCcAccessible(bool set)
{
    boolSetter(m_ccAccessible, set);
}

void Bug::setClassification(const QString& classification)
{
    stringSetter(m_classification, classification);
}

void Bug::setCommitLink(const QString& commitLink)
{
    stringSetter(m_commitLink, commitLink);
}

void Bug::setComponent(const QString& component)
{
    stringSetter(m_component, component);
}

void Bug::setConfirmed(bool set)
{
    boolSetter(m_confirmed, set);
}

void Bug::setCreationTime(const QString& creationTime)
{
    stringSetter(m_creationTime, creationTime);
}

void Bug::setCreator(const QString& creator)
{
    stringSetter(m_creator, creator);
}

void Bug::setCreatorAccessible(bool set)
{
    boolSetter(m_creatorAccessible, set);
}

void Bug::setDependsOn(const QVariantList& dependsOn)
{
    variantListSetter(m_dependsOn, dependsOn);
}

void Bug::setDupeOf(const QString& dupeOf)
{
    stringSetter(m_dupeOf, dupeOf);
}

void Bug::setGroups(const QVariantList& groups)
{
    variantListSetter(m_groups, groups);
}

void Bug::setId(qulonglong id)
{
    if (m_id == id) {
        return;
    }
    m_id = id;
}

void Bug::setKeywords(const QVariantList& keywords)
{
    variantListSetter(m_keywords, keywords);
}

void Bug::setLastChangeTime(const QString& lastChangeTime)
{
    stringSetter(m_lastChangeTime, lastChangeTime);
}

void Bug::setOpen(bool set)
{
    boolSetter(m_open, set);
}

void Bug::setOperatingSystem(const QString& operatingSystem)
{
    stringSetter(m_operatingSystem, operatingSystem);
}

void Bug::setPlatform(const QString& platform)
{
    stringSetter(m_platform, platform);
}

void Bug::setPriority(const QString& priority)
{
    stringSetter(m_priority, priority);
}

void Bug::setProduct(const QString& product)
{
    stringSetter(m_product, product);
}

void Bug::setQaContact(const QString& qaContact)
{
    stringSetter(m_qaContact, qaContact);
}

void Bug::setResolution(const QString& resolution)
{
    stringSetter(m_resolution, resolution);
}

void Bug::setSeeAlso(const QVariantList& seeAlso)
{
    variantListSetter(m_seeAlso, seeAlso);
}

void Bug::setSeverity(const QString& severity)
{
    stringSetter(m_severity, severity);
}

void Bug::setStatus(const QString& status)
{
    stringSetter(m_status, status);
}

void Bug::setSummary(const QString& summary)
{
    stringSetter(m_summary, summary);
}

void Bug::setTargetMilestone(const QString& targetMilestone)
{
    stringSetter(m_targetMilestone, targetMilestone);
}

void Bug::setUrl(const QString& url)
{
    stringSetter(m_url, url);
}

void Bug::setVersion(const QString& version)
{
    stringSetter(m_version, version);
}

void Bug::setVersionFixedIn(const QString& versionFixedIn)
{
    stringSetter(m_versionFixedIn, versionFixedIn);
}

void Bug::setWhiteboard(const QString& whiteboard)
{
    stringSetter(m_whiteboard, whiteboard);
}

QString Bug::commitHash() const
{
    if (!m_commitLink.isEmpty()) {
        QString hash = m_commitLink.split('/').last();
        if (!hash.isEmpty()) {
            return hash;
        }
    }
    QString hash;
    QDateTime hashDate;
    QRegExp commitRegExp("^Git commit (.*) by (.*)\\.\nCommitted on (.*) at ([0-9]+:[0-9]+)\\.(.*)$");
    for (auto child : children()) {
        if (auto comment = qobject_cast<Comment*>(child)) {
            if (comment->text().startsWith("Git commit")) {
                int pos = commitRegExp.indexIn(comment->text());
                if (pos > -1) {
                    QDateTime date = QDateTime::fromString(commitRegExp.cap(3) + " " + commitRegExp.cap(4), "dd/MM/yyyy HH:mm");
                    if (hash.isNull() || date > hashDate) {
                        hashDate = date;
                        hash = commitRegExp.cap(1);
                    }
                }
            }
        }
    }
    return hash;
}

QString Bug::reviewId() const
{
    if (m_url.isEmpty()) {
        return QString();
    }
    QRegExp reviewBoardRegExp("^http(s)*://(git|svn)\.reviewboard\.kde\.org/r/(.*)(/).*$");
    int pos = reviewBoardRegExp.indexIn(m_url);
    if (pos > -1) {
        return reviewBoardRegExp.cap(3);
    }
    return QString();
}

#include "bug.moc"
