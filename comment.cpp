/********************************************************************
Copyright (C) 2012 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "comment.h"
#include "bug.h"

Comment::Comment(Bug* parent)
    : QObject(parent)
    , m_bug(parent)
    , m_attachmentId(0)
    , m_author()
    , m_creator()
    , m_id(0)
    , m_private(false)
    , m_text()
    , m_time()
{
}

Comment::~Comment()
{
}

void Comment::setAttachmentId(qulonglong attachmentId)
{
    if (m_attachmentId == attachmentId) {
        return;
    }
    m_attachmentId = attachmentId;
}

auto stringSetter = [](QString &variable, const QString &value) {
    if (variable == value) {
        return false;
    }
    variable = value;
    return true;
};

void Comment::setAuthor(const QString& author)
{
    stringSetter(m_author, author);
}

void Comment::setCreator(const QString& creator)
{
    stringSetter(m_creator, creator);
}

void Comment::setId(qulonglong id)
{
    if (m_id == id) {
        return;
    }
    m_id == id;
}

void Comment::setPrivate(bool set)
{
    if (m_private == set) {
        return;
    }
    m_private = set;
}

void Comment::setText(const QString& text)
{
    stringSetter(m_text, text);
}

void Comment::setTime(const QString& time)
{
    stringSetter(m_time, time);
}

#include "comment.moc"
