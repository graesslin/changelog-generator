/********************************************************************
Copyright (C) 2012 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include <QtCore/QCoreApplication>
#include <QtCore/QStringList>

#include "ChangelogGenerator.h"

#include <iomanip>
#include <iostream>

static const QString KDE_BUGZILLA_URL = "https://bugs.kde.org";

void printHelp()
{
    auto printOption = [](const char *flag, const char *description) {
        std::cout << std::setw(30) << std::left << flag << description << std::endl;
    };
    std::cout << "KDE Changelog and Feature Plan generator" << std::endl << std::endl;

    printOption("--changelog",                  "Generate changelog XML, this is the default");
    printOption("--feature-plan",               "Generate MediaWiki syntax for feature plan on techbase.kde.org");
    printOption("-h, --help",                   "Shows this help");
    printOption("--module=",                    "Name of module in Changelog or feature plan");
    printOption("--product=<name>",             "Name of product in bugzilla, e.g. kwin");
    printOption("--repository=<name>",          "Name of repository in Changelog module");
    printOption("--target-milestone=<version>", "Search for bugs with specified target milestone");
    printOption("--url=<url>",                  "Bugzilla url, if not specified https://bugs.kde.org is used");
    printOption("--version=<version>",          "Search for bugs fixed in specified version");
}

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    QString bugzillaUrl, product, version, targetMilestone, module, repository;
    auto testArgument = [&](QString &variable, const QString &argumentStart, const QString &defaultValue = QString()) {
        for (const QString &argument : app.arguments()) {
            if (argument.startsWith(argumentStart)) {
                variable = argument.right(argument.length() - argumentStart.length());
                break;
            }
        }
        if (variable.isNull()) {
            variable = defaultValue;
        }
    };
    testArgument(bugzillaUrl, "--url=", KDE_BUGZILLA_URL);
    testArgument(product, "--product=");
    testArgument(version, "--version=");
    testArgument(targetMilestone, "--target-milestone=");
    testArgument(module, "--module=");
    testArgument(repository, "--repository=");
    ChangelogGenerator::Mode mode = ChangelogGenerator::Mode::Changelog;
    for (const QString &argument : app.arguments()) {
        if (argument == "--feature-plan") {
            mode = ChangelogGenerator::Mode::FeaturePlan;
            break;
        }
        if (argument == "--changelog") {
            mode = ChangelogGenerator::Mode::Changelog;
            break;
        }
        if (argument == "--help" || argument == "-h") {
            printHelp();
            return 0;
        }
    }
    ChangelogGenerator foo(mode);
    foo.setBugtracker(bugzillaUrl);
    foo.setProduct(product);
    foo.setVersion(version);
    foo.setTargetMilestone(targetMilestone);
    foo.setModule(module);
    foo.setRepository(repository);
    foo.start();
    return app.exec();
}
