/********************************************************************
Copyright (C) 2012 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#ifndef COMMENT_H
#define COMMENT_H

#include <QtCore/QObject>

class Bug;

class Comment : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qulonglong attachment_id READ attachmentId WRITE setAttachmentId)
    Q_PROPERTY(QString author READ author WRITE setAuthor)
    Q_PROPERTY(QString creator READ creator WRITE setCreator)
    Q_PROPERTY(qulonglong id READ id WRITE setId)
    Q_PROPERTY(bool is_private READ isPrivate WRITE setPrivate)
    Q_PROPERTY(QString text READ text WRITE setText)
    Q_PROPERTY(QString time READ time WRITE setTime)
public:
    explicit Comment(Bug* parent);
    virtual ~Comment();

    qulonglong attachmentId() const {
        return m_attachmentId;
    }
    const QString &author() const {
        return m_author;
    }
    const QString &creator() const {
        return m_creator;
    }
    qulonglong id() const {
        return m_id;
    }
    bool isPrivate() const {
        return m_private;
    }
    const QString &text() const {
        return m_text;
    }
    const QString &time() const {
        return m_time;
    }

    void setAttachmentId(qulonglong attachmentId);
    void setAuthor(const QString &author);
    void setCreator(const QString &creator);
    void setId(qulonglong id);
    void setPrivate(bool set);
    void setText(const QString &text);
    void setTime(const QString &time);

private:
    Bug *m_bug;
    qulonglong m_attachmentId;
    QString m_author;
    QString m_creator;
    qulonglong m_id;
    bool m_private;
    QString m_text;
    QString m_time;
};

#endif
